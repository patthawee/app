
import 'package:flutter/material.dart';
import 'main.dart';


void main() {
  runApp(MaterialApp(
    title: 'SHABU',
    debugShowCheckedModeBanner: false,
    home: FirstRoute(),
    theme: ThemeData(primarySwatch: Colors.yellow),
  ));
}
class FirstRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Container(
              child: Text(
                'WELLCOME TO SHABU',
                style: TextStyle(
                  fontSize: 50,
                  fontWeight: FontWeight.w900,
                  letterSpacing: 1,
                ),
              ),
            ),
            mainImage,
            Container(
              width: 440,
              child: ElevatedButton(
                child: Text('WELLCOME'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyApp()),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
final mainImage = Image.asset(
  'imges/1234.jpg',
  fit: BoxFit.cover,
  width: 1000,
);
import 'dart:html';
import 'package:flutter/material.dart';
import 'user.dart';
// Uncomment lines 7 and 10 to view the visual layout at runtime.
// import 'package:flutter/rendering.dart' show debugPaintSizeEnabled;

void main() {
  // debugPaintSizeEnabled = true;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Container(
                  padding: EdgeInsets.fromLTRB(635, 0, 600, 0),
                  child: Text(
                    'ยินดีต้อนรับเข้าสู่ระบบการจองโต๊ะร้านชาบูในพื้นที่จังหวัดภูเก็ต ท่านสามารถกรอกข้อมูลในหน้าถัดไปได้เลยครับ',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: 20,
                    ),
                  ),
                ),
              ],
            ),
          ),
          /*3*/
        ],
      ),
    );

    final iconList2 = DefaultTextStyle.merge(
//style: descTextStyle,
      child: Container(
        padding: EdgeInsets.all(20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                Icon(Icons.local_atm_rounded, color: Colors.yellow[500]),
                Text('STARTING PRICE:'),
                Text('289 ฿'),
              ],
            ),
            Column(
              children: [
                Icon(Icons.timer, color: Colors.pink[500]),
                Text('TIME:'),
                Text('NO TIME NIMIT'),
              ],
            ),
            Column(
              children: [
                Icon(Icons.local_dining_rounded, color: Colors.red[500]),
                Text('OPEN:'),
                Text('11.00-23.00'),
              ],
            ),
          ],
        ),
      ),
    );

    return MaterialApp(
      title: 'WELLCOME TO SHABU',
      home: Scaffold(
        appBar: AppBar(
          title: Text('WELLCOME TO SHABU'),
          backgroundColor: (Colors.pink),
        ),
        body: ListView(
          children: [
            Image.asset(
              'imges/111.jpg',
              width: 100,
              height: 540,
              fit: BoxFit.cover,
            ),
            titleSection,
            iconList2,
            ElevatedButton(
                child: Text('next'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Home()),
                  );
                }),
          ],
        ),
      ),
    );
  }
}
